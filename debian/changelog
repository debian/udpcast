udpcast (20120424-2) UNRELEASED; urgency=medium

  * Migrate repository from alioth to salsa.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 27 May 2022 20:51:48 +0100

udpcast (20120424-1) unstable; urgency=low

  * New upstream version.
  * Update years and Format: URL in debian/copyright.
  * Build-depend on and use dh-autoreconf to generate new versions of outdated
    autotools files.
  * Increase debian/compat to 9 and adjust the build-dependency on debhelper
    accordingly.
  * Bump Standards-Version to 3.9.4 (no changes needed).
  * Use a host part of anonscm.debian.org instead of git.debian.org in the
    debian/control VCS pointers.
  * Rename Debian patches to *.diff.
  * New patch missing_fd_set.diff: #include <sys/select.h> in console.h so the
    definition of the fd_set data type is available, closes: #701436.  Thanks
    to Daniel T Chen for the patch.

 -- Michael Schutte <michi@debian.org>  Fri, 10 May 2013 15:23:55 +0200

udpcast (20100130-3) unstable; urgency=low

  * Update maintainer e-mail address to michi@debian.org.
  * Bump Standards-Version to 3.9.2 (no changes needed).
  * Format debian/copyright header according to DEP-5.
  * New patch manpage_fixes: Remove some spelling errors kindly pointed out by
    lintian.

 -- Michael Schutte <michi@debian.org>  Sat, 23 Jul 2011 12:26:40 +0200

udpcast (20100130-2) unstable; urgency=low

  * Ship examples/ to demonstrate the implementation of a rate governor.
  * New patch nostrip: Handle the DEB_BUILD_OPTIONS value by the same
    name, closes: #438231.
  * Remove cmd.html.man in debian/rules clean.

 -- Michael Schutte <michi@uiae.at>  Mon, 03 May 2010 19:11:48 +0200

udpcast (20100130-1) unstable; urgency=low

  * Adopt this package, closes: #575167.
  * New upstream release, closes: #375682.
    - printMyIp() in socklib.c now uses a proper format string to
      udpc_flprintf(), closes: #387169.
    - Build-depend on m4.
  * Use debhelper 7, change build-dependency, update debian/compat and
    greatly shorten debian/rules.
  * Add ${misc:Depends}.
  * Bump Standards-Version to 3.8.4, no changes needed.
  * Convert package to the 3.0 (quilt) format.
  * Provide a debian/watch file.
  * Convert debian/copyright to the machine-readable format as proposed in
    DEP 5 and update the information.
  * Upload package to a collab-maint Git repository and add Vcs-* fields in
    debian/control.
  * Point to the project homepage in debian/control.

  [Tomas Pospisek]
  * thanks to Alain Knaff for providing Debian packages for all these
    years

 -- Michael Schutte <michi@uiae.at>  Sat, 17 Apr 2010 14:03:19 +0200

udpcast (20040531-3) unstable; urgency=low

  * fixed lintian's copyright-lists-upstream-authors-with-dh_make-boilerplate
  * orphaning package, since upstream has been producing packages since
    2007 and I have neither been using udpcast nor maintaining it.
  * therefore setting maintainer to Debian QA

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Tue, 23 Mar 2010 23:09:30 +0100

udpcast (20040531-2) unstable; urgency=low

  * fixed lintian's copyright-lists-upstream-authors-with-dh_make-boilerplate
  * NOT RELEASED

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Mon, 06 Sep 2004 16:58:38 +0200

udpcast (20040531-1) unstable; urgency=low

  * new upstream

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Wed, 02 Jun 2004 10:30:42 +0200

udpcast (20030831-1) unstable; urgency=low

  * new upstream                                            closes: #221526

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Sat, 22 Nov 2003 00:17:00 +0100

udpcast (20030706-1) unstable; urgency=low

  * new alpha upstream:                                     closes: #203815
    * new features: --retriesUntilDrop, --nokbd, --exitWait
  * Debian:
    * closed in unreleased version (20030601-1):
      closes: #187622, closes: #187619, closes: #187626

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Thu, 05 Dec 2002 10:34:37 +0100

udpcast (20030601-1) unstable; urgency=low

  * new alpha upstream
    * unattended mode
    * max clients = 1024
    * (both patches by Gary Skouson and Brian Finley)       closes: #187622
    * can be compiled into busybox (not used in the Debian package)
    * robuster against bad packets
    * bugfixes                                              closes: #187619
    * more logging                                          closes: #187626
  * Debian:
    * now assembler code should really be active on i386 platforms

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Thu, 05 Dec 2002 10:34:37 +0100

udpcast (20011231-6) unstable; urgency=low

  * changed path of udp-[sender|receiver] to /usr/bin       closes: #172676

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Thu, 05 Dec 2002 10:34:37 +0100

udpcast (20011231-5) unstable; urgency=low

  * fixed typo in description
  * enablying assembler code on i386 only                   closes: #172039

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Thu, 05 Dec 2002 10:34:37 +0100

udpcast (20011231-4) unstable; urgency=low

  * tar wasn't included in changes -> retry upload

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Thu, 05 Dec 2002 10:34:37 +0100

udpcast (20011231-3) unstable; urgency=low

  * removed usr/bin from dirs
  * signed with my debian key instead of some other -> retry upload

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Thu, 05 Dec 2002 09:45:42 +0100

udpcast (20011231-2) unstable; urgency=low

  * removed cruft from first upload :-/

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Wed, 04 Dec 2002 18:40:05 +0100

udpcast (20011231-1) unstable; urgency=low

  * Initial Release.

 -- Tomas Pospisek <tpo_deb@sourcepole.ch>  Wed,  4 Dec 2002 16:00:21 +0100

